Rails.application.routes.draw do

	root 'articles#index'
	resources :articles
	get 'baru', to: 'articles#new'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
